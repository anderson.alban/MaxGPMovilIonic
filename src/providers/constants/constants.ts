export const Constants = {
  versioApp: '1.0.0',
  opcionesMenuPrincipal: [
    {
      iconoPanel: '/assets/iconos/clientesBlanco.svg',
      iconoMenu: '/assets/iconos/clientesGrisOscuro.svg',
      nombre: 'Inicio',
      colorBackground: '#1a75bc',
      routerLink: '/panel-principal'
    },
    {
      iconoPanel: '/assets/iconos/clientesBlanco.svg',
      iconoMenu: '/assets/iconos/clientesGrisOscuro.svg',
      nombre: 'Clientes',
      colorBackground: '#1a75bc',
    },
    {
      iconoPanel: '/assets/iconos/tareasBlanco.svg',
      iconoMenu: '/assets/iconos/tareasGrisOscuro.svg',
      nombre: 'Tareas',
      colorBackground: '#b3c029'
    },
    {
      iconoPanel: '/assets/iconos/calendarioBlanco.svg',
      iconoMenu: '/assets/iconos/calendarioGrisOscuro.svg',
      nombre: 'Calendario',
      colorBackground: '#d69a07',
      routerLink: '/calendario'
    },
    {
      iconoPanel: '/assets/iconos/pedidosBlanco.svg',
      iconoMenu: '/assets/iconos/pedidosGrisOscuro.svg',
      nombre: 'Pedidos',
      colorBackground: '#ed830f'
    },
    {
      iconoPanel: '/assets/iconos/sincronizarBlanco.svg',
      iconoMenu: '/assets/iconos/sincronizarGrisOscuro.svg',
      nombre: 'Seincronización',
      colorBackground: '#6ba313'
    },
    {
      iconoPanel: '/assets/iconos/cerrarSesionBlanco.svg',
      iconoMenu: '/assets/iconos/cerrarSesionGrisOscuro.svg',
      nombre: 'Cerrar Sesión',
      colorBackground: '#e50505',
      routerLink: '/login'
    }
  ]
};  