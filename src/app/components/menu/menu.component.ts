import { Component, OnInit } from '@angular/core';
import { Constants } from '../../../providers/constants/constants';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
  constants = Constants;
  opcionesMenuPrincipal: any = this.constants.opcionesMenuPrincipal;
  
  constructor() { }
  
  ngOnInit() {
  }

}
