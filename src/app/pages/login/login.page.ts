import { Component, OnInit } from '@angular/core';
// import { AuthService } from '../../services/auth/auth.service';
import { DatabaseService } from '../../services/database/database.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(
    // private auth: AuthService,
    private db: DatabaseService
  ) { }

  ngOnInit() {
    this.PruebasServicioLogin();
  }

  PruebasServicioLogin() {
    this.db.login().then(data => {
      console.log(data);
    });
  }

}
