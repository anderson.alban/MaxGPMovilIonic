import { Component, OnInit } from '@angular/core';
import { Constants } from '../../../providers/constants/constants';

@Component({
  selector: 'app-panel-principal',
  templateUrl: './panel-principal.page.html',
  styleUrls: ['./panel-principal.page.scss'],
})
export class PanelPrincipalPage implements OnInit {
  constants = Constants;
  opcionesMenuPrincipal: any = this.constants.opcionesMenuPrincipal;
  constructor() { }

  ngOnInit() {
    // this.opcionesMenuPrincipal.splice(1);
    this.opcionesMenuPrincipal = this.opcionesMenuPrincipal.filter((data) => {
      return data.nombre != 'Inicio';
    });
  }

}
