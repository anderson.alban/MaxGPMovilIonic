import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { HttpClient } from '@angular/common/http';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {
  private database: SQLiteObject;
  private dbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(
    private plt: Platform,
    private sqlitePorter: SQLitePorter,
    private sqlite: SQLite,
    private http: HttpClient
  ) {
    this.plt.ready().then(() => {
      this.sqlite.create({
        name: 'maxgp.sql',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.database = db;
          this.seedDatabase();
        });
    });
  }

  /**
   * @description Se encarga de hacer la primer sincronizacion de la base de datos (Estructura y datos base) de MaxGp
   */
  seedDatabase() {
    this.http.get('assets/maxgp.sql', { responseType: 'text' })
      .subscribe(sql => {
        this.sqlitePorter.importSqlToDb(this.database, sql)
          .then(data => {
            // Define de forma global si la base de datos esta cargada
            this.dbReady.next(true);
          })
          .catch(e => console.error(e));
      });
  }

  /**
   * @description Define el estado a la base de datos true ready falsa no tiene conexión
   */
  getDatabaseState() {
    return this.dbReady.asObservable();
  }

  login() {

    return this.database.executeSql('SELECT * FROM GT_USUARIOS', []).then(data => {
      let users = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < 10; i++) {
          users.push({
            numero: data.rows.item(i).NUMERO,
            nombre: data.rows.item(i).NOMBRE,
            apellido: data.rows.item(i).APELLIDO
          });
        }
      }
      return users;
    });

  }
}
