import { Injectable } from '@angular/core';
import { DatabaseService } from '../database/database.service';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { HttpClient } from '@angular/common/http';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private database: SQLiteObject;
  private dbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(
    private db: DatabaseService
  ) { }

  login() {
    return this.database.executeSql('SELECT * FROM GT_USUARIOS', []).then(data => {
      let users = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < 10; i++) {
          users.push({
            nombre: data.rows.item(i).NOMBRE,
            apellido: data.rows.item(i).APELLIDO,
          });
        }
      }
      return users;
    });
  }
}
